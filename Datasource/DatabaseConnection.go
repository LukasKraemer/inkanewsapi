package Datasource

import (
	"database/sql"
	"fmt"
	"os"
)

func Connect() *sql.DB {
	var DB *sql.DB
	DB, err := sql.Open("mysql", os.Getenv("databaseString"))

	if err != nil{
		if DB == nil {
			fmt.Println(err.Error())
			os.Exit(35)
		}
	}
		return DB
}