package Datasource

type LogEntry struct {
	Idlog         int     	`json:"id"`
	UserId        int     	`json:"user"`
	Date          int     	`json:"date"`
	Change 		  int  		`json:"activityType"`
	Appointment   int		`json:"changes"`
}

type Appointment struct {
	Idappointment        int     `json:"id"`
	Date          int     `json:"date"`
	Notice     string `json:"notice"`
	State  int  `json:"state"`
	Type int     `json:"type"`
	Group int     `json:"group"`
}

type Profile struct {
	Iduser int
	Email string
	Passwd string
	Notice string
	name string
	surname string
	CreateDate int
	Emailnotification int
	Emailconfirmed int
	State int
}

type User struct {
	Iduser int
	Email string
	Passwd string
	State int
}