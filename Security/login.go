package Security

import (
	"INkaNewsAPI/Datasource"
	"encoding/base64"
	"encoding/hex"
	"fmt"
	"golang.org/x/crypto/sha3"
	"net/http"
	"strings"
)

func basicAuth(w http.ResponseWriter, r *http.Request) ( Datasource.User, bool) {
	user := Datasource.User{}
	validated := false
	auth := strings.SplitN(r.Header.Get("Authorization"), " ", 2)

	if len(auth) != 2 || auth[0] != "Basic" {
		return user,false
	}

	payload, _ := base64.StdEncoding.DecodeString(auth[1])
	pair := strings.SplitN(string(payload), ":", 2)

	validated, user = validate(pair[0], pair[1])
	if len(pair) != 2 || !validated {
		return Datasource.User{}, false
	}else{
		return user, true
	}
}

func validate(name string, passwd string) (bool, Datasource.User) {
	var user Datasource.User
	db := Datasource.Connect()
	h := sha3.New512()
	h.Write([]byte(passwd))
	sum := hex.EncodeToString( h.Sum(nil))
	fmt.Println( sum)

	stmt, err := db.Prepare("SELECT iduser, email, passwd, state_id FROM user where email = ? AND passwd = ? AND state_id = 1;")
	if err != nil{
		panic( err.Error())
		return false, user
	}

	res, err1 := stmt.Query(name,sum)
	if err1 != nil{
		panic( err1.Error())
		return false, user
	}
	res.Next()
	res.Scan(&user.Iduser, &user.Email, &user.Passwd, &user.State)
	if user.Iduser != 0{
		return true, user
	}else{
		return false, user
	}


}
