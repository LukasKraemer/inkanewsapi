package Security

import (
	"INkaNewsAPI/Datasource"
	_ "github.com/go-sql-driver/mysql"
	"net/http"
	"time"
)

func Prepare(change int, appointmentid int, w http.ResponseWriter, r *http.Request ) (bool, Datasource.User) {
	userdata, valid := basicAuth(w,r)
	if !valid{
		http.Error(w, "authorization failed", http.StatusUnauthorized)
		return false, Datasource.User{}
	}else{
		logger(userdata.Iduser,change,appointmentid)
		return true, userdata
	}
}

func logger(userid int, change int, appointmentid int){
	date := time.Now().Unix()
	db := Datasource.Connect()
	insert, err1 := db.Prepare("INSERT INTO log (`userid`, `date`, `change`, `appointment`) VALUES (?, ? , ?, ?)")
	if err1 != nil {
		return
	}
	_, err2 := insert.Exec(userid,  date, change, appointmentid)
	if err2 != nil {
		panic(err2.Error())
		return
	}
}