package Gateway

import (
	"INkaNewsAPI/Datasource"
	"database/sql"
	"encoding/json"
	"net/http"
)

func ListAllLogs(writer http.ResponseWriter, request *http.Request) {
	writer.Header().Set("Content-Type", "application/json")
	var logs []Datasource.LogEntry
	db := Datasource.Connect()

	result, err := db.Query("SELECT * FROM log;")
	defer func(result *sql.Rows) {
		err := result.Close()
		if err != nil {

		}
	}(result)
	if err != nil {
		return
	}
	for result.Next() {
		var log Datasource.LogEntry
		result.Scan(&log.Idlog, &log.UserId, &log.Date, &log.Change, &log.Appointment)
		logs = append(logs, log)
	}
	err = json.NewEncoder(writer).Encode(logs)
}
