package Gateway

import (
	"INkaNewsAPI/Datasource"
	"INkaNewsAPI/Security"
	"database/sql"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"net/http"
	"strconv"
)

func LoadTask(writer http.ResponseWriter, request *http.Request) {
	valid, _ := Security.Prepare(5,0, writer, request)
	if !valid{
		return
	}
	writer.Header().Set("Content-Type", "application/json")
	var all []Datasource.Appointment
	db := Datasource.Connect()
	result, err := db.Query("SELECT * FROM appointment; ")
	defer func(result *sql.Rows) {
		err := result.Close()
		if err != nil {

		}
	}(result)

	if err != nil {
		return
	}
	for result.Next() {
		var appointment Datasource.Appointment
		result.Scan(&appointment.Idappointment, &appointment.Date, &appointment.Notice, &appointment.State, &appointment.Type, &appointment.Group)
		all = append(all, appointment)
	}
	err = json.NewEncoder(writer).Encode(all)
}

func ListOneTask(writer http.ResponseWriter, request *http.Request) {
	writer.Header().Set("Content-Type", "application/json")
	params := mux.Vars(request) // Gets params
	id, _ := strconv.Atoi(params["id"])
	valid , _ := Security.Prepare(5,id, writer, request)
	if !valid{
		return
	}
	db := Datasource.Connect()
	stmt, err1 := db.Prepare("SELECT * FROM appointment where idappointment = ?; ")
	if err1 != nil {
		return
	}
	result, err2 := stmt.Query(id)
	defer func(stmt *sql.Stmt) {
		err := stmt.Close()
		if err != nil {

		}
	}(stmt)

	if err2 != nil {
		return
	}

	for result.Next() {
		var appointment Datasource.Appointment
		result.Scan(&appointment.Idappointment, &appointment.Date, &appointment.Notice, &appointment.State, &appointment.Type, &appointment.Group)
		err3 := json.NewEncoder(writer).Encode(appointment)
		if err3 != nil{
			return
		}
	}


}

func AddTask(writer http.ResponseWriter, request *http.Request) {
	writer.Header().Set("Content-Type", "application/json")

	var appointment Datasource.Appointment
	json.NewDecoder(request.Body).Decode(&appointment)

	db := Datasource.Connect()
	res,_ := db.Exec("INSERT INTO appointment (date, notice, state, type, group_id) VALUES (?, ?, ?, ?, ?);",
		appointment.Date, appointment.Notice, appointment.State, appointment.Type, appointment.Group)
	id, err := res.LastInsertId()
	if err != nil{
		return
	}else{
		valid, _ := Security.Prepare(1,int(id), writer, request)
		if !valid{
			return
		}
		json.NewEncoder(writer).Encode(id)
	}
}

func ChangeTask(writer http.ResponseWriter, request *http.Request) {
	writer.Header().Set("Content-Type", "application/json")

	var appointment Datasource.Appointment
	err := json.NewDecoder(request.Body).Decode(&appointment)

	if err != nil{return}
	db := Datasource.Connect()
	res,_ := db.Exec("UPDATE appointment SET date = ?, notice =?, state = ?, type = ?, group_id = ? WHERE (idappointment = ?);",
		appointment.Date, appointment.Notice, appointment.State, appointment.Type, appointment.Group, appointment.Idappointment)

	id, err1 := res.RowsAffected()
	if err1 != nil{
		return
	}else{
		valid, _ :=Security.Prepare(4,int(id), writer, request)
		if !valid{
			return
		}
		json.NewEncoder(writer).Encode(id)
	}
}

func RemoveTask(writer http.ResponseWriter, request *http.Request) {
	writer.Header().Set("Content-Type", "application/json")
	params := mux.Vars(request) // Gets params
	id, _ := strconv.Atoi(params["id"])
	valid, _ :=Security.Prepare(3,int(id), writer, request)
	if !valid{
		return
	}
	db := Datasource.Connect()
	res,_ := db.Exec("DELETE FROM appointment WHERE (idappointment = ?);", id)
	aff, err2 := res.RowsAffected()
	if  aff == 1 && err2 == nil {
		fmt.Fprint(writer, "DELETE Appointment nr " +  params["id"])
	}else{
		fmt.Fprint(writer, "ERROR WHILE DELETE Appointment nr " +  params["id"])
	}
}
