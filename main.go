package main

import (
	"INkaNewsAPI/Gateway"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
	"github.com/joho/godotenv"
	"log"
	"net/http"
)


func handleRequests() {
	r := mux.NewRouter()

	r.HandleFunc("/task", Gateway.LoadTask).Methods("GET")
	r.HandleFunc("/task/{id}", Gateway.ListOneTask).Methods("GET")
	r.HandleFunc("/task", Gateway.AddTask).Methods("POST")
	r.HandleFunc("/task/{id}", Gateway.ChangeTask).Methods("PUT")
	r.HandleFunc("/task/{id}", Gateway.RemoveTask).Methods("DELETE")

	r.HandleFunc("/logs", Gateway.ListAllLogs).Methods("GET")

	log.Fatal(http.ListenAndServe(":10000", r))
}

func main() {
	fmt.Println("Rest API for INKA NEWS")
	err := godotenv.Load()
	if err != nil {
		return
	}
	handleRequests()
}